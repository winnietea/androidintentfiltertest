package com.winnie.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mTextWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextWord = (TextView) findViewById(R.id.txt);

        String s = getIntent().toURI();
        Log.d("MyActivity", "Query get from intent : " + s);

        Uri uri = getIntent().getData(); //Returns the URI that was used.
        if(uri != null) {
            String params = uri.getQueryParameter("params"); //Gives any parameter passed in the URI.
            if (isValid(params)) {
                mTextWord.setText(params);
            }else{
                if(isValid(s)){
                    mTextWord.setText(s);
                }
            }
        }

    }

    private boolean isValid(String params){
        return params != null && params.length() > 0;
    }

}
